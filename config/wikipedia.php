<?php

return [

    'base_url' => 'https://en.wikipedia.org/api/rest_v1/',

    'articles' => [
        'gb' => 'United_Kingdom',
        'nl' => 'Netherlands',
        'de' => 'Germany',
        'fr' => 'France',
        'es' => 'Spain',
        'it' => 'Italy',
        'gr' => 'Greece'
    ]

];