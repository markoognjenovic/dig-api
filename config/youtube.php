<?php

return [

    'base_url' => 'https://www.googleapis.com/youtube/v3/',

    'api_key' => env('YOUTUBE_API_KEY', ''),

    /*
     * You are free to add or remove some fields when fetching from YouTube API.
     * Note that it will automatically reflect REST API response JSON format.
     * You can read about fields construction rules:
     * https://developers.google.com/youtube/v3/getting-started#fields
     */
    'fields' => 'etag,items(id,snippet/title,snippet/description,snippet/thumbnails/standard,snippet/thumbnails/high)'

];