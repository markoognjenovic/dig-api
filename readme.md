# DIG - API Assessment
## REST API

### Installation and Setup

1 - Clone the repository on your local machine.

```shell
$ git clone https://gitlab.com/markoognjenovic/dig-api.git
```
 
2 - Copy and configure .env:

```shell
$ cd dig-api
$ cp .env.example .env
```

**_Important_**: In this step you will need to add your YouTube API key in .env file variable named **YOUTUBE_API_KEY**

3 - Run dependency manager

```shell
$ composer install
```

#### Requirements

In order to work properly, project requires following:

- Running web server with PHP >=7.1.3
- Running Redis server

### Overview

Application is designed to fetch data from two different resources (YouTube and Wikipedia), and provide a JSON representation of those data per countries.

#### Endpoints:

GET **/api/v1** - Displays array of countries with their foreword and most popular YouTube videos.

#### Technical:

There are two commands for fetching resources. One for YouTube and one for Wikipedia. You can run them as follows:

```shell
$ php artisan fetch:youtube
$ php artisan fetch:wikipedia
```

Those commands will execute external data fetching from those resources, and store it in Redis database for fast access.

Commands are separated, so it is easy to shedule different cron job intervals. When you run sheduler, it will fire those commands in following intervals:
1. Wikipedia - once a day
2. YouTube - once an hour

If you don't run this commands on project setup, it will automatically fire on first API call. Application is smart enough to fire those commands every time it encounter missing data for specific countries. Commands accepts "countries" as an argument, or fetches all by default.

#### Specific configuration

1 - Change API supported countries:

**config/app.php**

```shell
'countries' => ['gb', 'nl', 'de', 'fr', 'es', 'it', 'gr']
```

2 - Change Wikipedia articles by country:

**config/wikipedia.php**

```shell
'articles' => [
    'gb' => 'United_Kingdom',
    'nl' => 'Netherlands',
    'de' => 'Germany',
    'fr' => 'France',
    'es' => 'Spain',
    'it' => 'Italy',
    'gr' => 'Greece'
]
```

3 - Change YouTube video data fields:

**config/youtube.php**

```shell
'fields' => 'etag,items(id,snippet/title,snippet/description,snippet/thumbnails/standard,snippet/thumbnails/high)'
```
You can read about [fields construction rules](https://developers.google.com/youtube/v3/getting-started#fields)

**_Important_**: You need to run following command after YouTube **fields** configuration changements:

```shell
$ php artisan fetch:youtube
```

### Tests

Project contains unit and feature tests (41 in total).

### Docker

Project have included docker-compose file for easy docker setup.