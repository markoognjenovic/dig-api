<?php

namespace Tests\Feature;

use App\Contracts\WikipediaRepositoryInterface;
use App\Contracts\YoutubeRepositoryInterface;
use App\Repositories\WikipediaRepository;
use App\Repositories\YoutubeRepository;
use Faker\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;
use Mockery;

class GetTest extends TestCase
{
    protected $url;

    public function setUp(): void
    {
        parent::setUp();

        $this->url = '/api/v1/';
    }

    /**
     * @test
     */
    function valid_request_without_parameters()
    {
        $faker = Factory::create();

        $foreword = $faker->realText(3000);
        $videos = [
            ['id' => $faker->uuid],
            ['id' => $faker->uuid]
        ];

        $this->bindYoutubeRepository([
            'etag' => $faker->md5,
            'data' => $videos
        ]);
        $this->bindWikipediaRepository([
            'etag' => $faker->md5,
            'data' => $foreword
        ]);

        $this->json('GET', $this->url);

        $response = json_decode($this->response->content(), true);

        $this->assertEquals(count($response['data']), count(Config::get('app.countries')));

        foreach ($response['data'] as $data) {
            $this->assertTrue(isset($data['country']));
            $this->assertTrue(isset($data['foreword']));
            $this->assertTrue(isset($data['videos']));
        }

        $this->assertResponseStatus(JsonResponse::HTTP_OK);

    }

    private function bindYoutubeRepository($data = null)
    {
        $faker = Factory::create();

        if (!isset($data)) {
            $data = [
                ['id' => $faker->uuid],
                ['id' => $faker->uuid]
            ];
        }

        $youtubeRepository = Mockery::mock(YoutubeRepository::class);
        $youtubeRepository->shouldReceive('getByCountry')->andReturn($data);
        app()->bind(YoutubeRepositoryInterface::class, function () use ($youtubeRepository) {
            return $youtubeRepository;
        });
    }

    private function bindWikipediaRepository($data = null)
    {
        $faker = Factory::create();

        if (!isset($data)) {
            $data = [
                'etag' => $faker->md5,
                'data' => $faker->realText(3000)
            ];
        }

        $wikipediaRepository = Mockery::mock(WikipediaRepository::class);
        $wikipediaRepository->shouldReceive('getByCountry')->andReturn($data);
        app()->bind(WikipediaRepositoryInterface::class, function () use ($wikipediaRepository) {
            return $wikipediaRepository;
        });
    }

    /**
     * @test
     */
    function valid_request_with_valid_pagination()
    {

        $limit = min(count(Config::get('app.countries')), 3);

        $this->bindYoutubeRepository();
        $this->bindWikipediaRepository();

        $this->json('GET', $this->url, ['limit' => $limit, 'offset' => 0]);

        $response = json_decode($this->response->content());

        $this->assertEquals(count($response->data), $limit);

        $this->assertResponseStatus(JsonResponse::HTTP_OK);

    }

    /**
     * @test
     * @dataProvider dataProvider
     */
    function valid_request_with_invalid_pagination($limit, $offset)
    {
        $this->bindYoutubeRepository();
        $this->bindWikipediaRepository();

        $this->json('GET', $this->url, ['limit' => $limit, 'offset' => $offset]);

        $this->assertResponseStatus(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    function valid_request_with_large_limit()
    {
        $count = count(Config::get('app.countries'));

        $this->bindYoutubeRepository();
        $this->bindWikipediaRepository();

        $this->json('GET', $this->url, ['limit' => $count + 10]);

        $response = json_decode($this->response->content());

        $this->assertEquals(count($response->data), $count);

        $this->assertResponseStatus(JsonResponse::HTTP_OK);
    }

    /**
     * @test
     */
    function valid_request_with_large_offset()
    {
        $count = count(Config::get('app.countries'));

        $this->bindYoutubeRepository();
        $this->bindWikipediaRepository();

        $this->json('GET', $this->url, ['offset' => $count + 10]);

        $response = json_decode($this->response->content());

        $this->assertEquals(count($response->data), 0);

        $this->assertResponseStatus(JsonResponse::HTTP_OK);
    }

    public function dataProvider()
    {
        return [
            [-1, -2],
            [0, 1],
            [1, -1],
            ['dsa', 'sda']
        ];
    }
}