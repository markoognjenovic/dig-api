<?php

namespace Tests\Unit\Services\DataFetcher;


use App\Console\Commands\FetchWikipedia;
use App\Console\Commands\FetchYoutube;
use App\Repositories\RedisRepository;
use App\Services\DataFetcher;
use Faker\Factory;
use Tests\TestCase;
use Mockery;

class FetchMethodTest extends TestCase
{
    /**
     * @test
     */
    public function fetch_data_all_countries_in_cache()
    {
        $faker = Factory::create();

        $dataToReturn = [
            'de' => ['country' => 'de', 'foreword' => $faker->realText(3000), 'videos' => [['id' => $faker->uuid]]],
            'it' => ['country' => 'it', 'foreword' => $faker->realText(3000), 'videos' => [['id' => $faker->uuid]]]
        ];

        $redisRepository = Mockery::mock(RedisRepository::class);
        $redisRepository->shouldReceive('getByCountries')->once()
            ->andReturn($dataToReturn);

        $youtubeCommand = Mockery::mock(FetchYoutube::class);
        $youtubeCommand->shouldNotReceive('handle');

        $wikipediaCommand = Mockery::mock(FetchWikipedia::class);
        $wikipediaCommand->shouldNotReceive('handle');

        $dataFetcher = new DataFetcher($redisRepository);
        $data = $dataFetcher->fetch(['de', 'it']);

        $this->assertSame($data, array_values($dataToReturn));
    }

    /**
     * @test
     */
    public function fetch_data_missing_countries_from_cache()
    {
        $faker = Factory::create();

        $fakeForeword = $faker->realText(3000);
        $fakeId = $faker->uuid;

        $dataToReturn = [
            'de' => ['country' => 'de', 'foreword' => $fakeForeword, 'videos' => [['id' => $fakeId]]],
            'it' => ['country' => 'it', 'foreword' => $fakeForeword, 'videos' => [['id' => $fakeId]]]
        ];

        $redisRepository = Mockery::mock(RedisRepository::class);
        $redisRepository->shouldReceive('getByCountries')->twice()
            ->andReturn(
                ['de' => $dataToReturn['de']],
                ['it' => $dataToReturn['it']]
            );
        $redisRepository->shouldReceive('clearDataForCountries');

        $youtubeCommand = Mockery::mock(FetchYoutube::class);
        $youtubeCommand->shouldReceive('handle');

        $wikipediaCommand = Mockery::mock(FetchWikipedia::class);
        $wikipediaCommand->shouldReceive('handle');

        $dataFetcher = new DataFetcher($redisRepository);
        $data = $dataFetcher->fetch(['de', 'it']);

        $this->assertSame($data, array_values($dataToReturn));
    }
}