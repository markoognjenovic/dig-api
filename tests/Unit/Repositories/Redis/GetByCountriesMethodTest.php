<?php

namespace Tests\Unit\Repositories\Redis;


use App\Repositories\RedisRepository;
use Faker\Factory;
use Illuminate\Redis\RedisManager;
use Tests\TestCase;
use Mockery;

class GetByCountriesMethodTest extends TestCase
{
    /**
     * @test
     */
    public function get_by_countries_valid()
    {
        $faker = Factory::create();

        $fakeForeword = $faker->realText(3000);
        $fakeId = $faker->uuid;
        $fakeVideos = [
            ['id' => $fakeId, 'snippet' => ['title' => $faker->realText()]]
        ];
        $fakeVidesJson = json_encode($fakeVideos);
        $countries = ['de', 'it'];

        $redisManager = Mockery::mock(RedisManager::class);
        $redisManager->shouldReceive('hget')->times(4)
            ->andReturn($fakeForeword, $fakeVidesJson, $fakeForeword, $fakeVidesJson);

        $redisRepository = new RedisRepository($redisManager);

        $result = $redisRepository->getByCountries(['de', 'it']);

        $this->assertSame(array_keys($result), $countries);
        $this->assertEquals(count($result), 2);
        $this->assertEquals($result['de']['foreword'], $fakeForeword);
        $this->assertEquals($result['de']['videos'][0]['id'], $fakeId);
    }

    /**
     * @test
     */
    public function get_by_countries_empty_database()
    {
        $countries = ['de', 'it'];
        $redisManager = Mockery::mock(RedisManager::class);
        $redisManager->shouldReceive('hget')->times(4)
            ->andReturn(null, null, null, null);

        $redisRepository = new RedisRepository($redisManager);

        $result = $redisRepository->getByCountries(['de', 'it']);

        $this->assertSame(array_keys($result), $countries);
        $this->assertEquals(count($result), 2);
        $this->assertEquals($result['de']['foreword'], null);
        $this->assertEquals($result['de']['videos'], null);
    }

    /**
     * @test
     */
    public function get_by_countries_partial_data()
    {
        $faker = Factory::create();

        $fakeForeword = $faker->realText(3000);

        $countries = ['de', 'it'];
        $redisManager = Mockery::mock(RedisManager::class);
        $redisManager->shouldReceive('hget')->times(4)
            ->andReturn($fakeForeword, null, $fakeForeword, null);

        $redisRepository = new RedisRepository($redisManager);

        $result = $redisRepository->getByCountries(['de', 'it']);

        $this->assertSame(array_keys($result), $countries);
        $this->assertEquals(count($result), 2);
        $this->assertEquals($result['de']['foreword'], $fakeForeword);
        $this->assertEquals($result['de']['videos'], null);
    }

}