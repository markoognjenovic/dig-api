<?php

namespace Tests\Unit\Repositories\Youtube;


use App\Exceptions\RepositoryException;
use App\Repositories\YoutubeRepository;
use App\Transformers\YoutubeResponseTransformer;
use Faker\Factory;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Mockery;

class GetByCountryMethodTest extends TestCase
{
    /**
     * @test
     * @throws RepositoryException
     */
    public function get_by_country_valid()
    {
        $faker = Factory::create();

        $fakeEtag = $faker->md5;
        $fakeKey = $faker->md5;
        $fakeId = $faker->uuid;
        $fakeTitle = $faker->realText(50);

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(200, [], json_encode([
                'etag' => $fakeEtag,
                'items' => [
                    [
                        'id' => $fakeId,
                        'snippet' => [
                            'title' => $fakeTitle
                        ]
                    ],
                    [
                        'id' => $fakeId,
                        'snippet' => [
                            'title' => $fakeTitle
                        ]
                    ],
                ]
            ]))
        );

        $youtubeRepository = new YoutubeRepository($client, new YoutubeResponseTransformer(), $fakeKey,
            'etag,items(id,snippet/title)');

        $result = $youtubeRepository->getByCountry('de');

        $this->assertEquals($result['etag'], $fakeEtag);
        $this->assertEquals($result['data'][0]['id'], $fakeId);
        $this->assertEquals($result['data'][0]['snippet']['title'], $fakeTitle);
        $this->assertEquals(count($result['data']), 2);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function third_party_good_response_but_empty()
    {
        $faker = Factory::create();

        $fakeKey = $faker->md5;

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(200, [], json_encode([]))
        );

        $youtubeRepository = new YoutubeRepository($client, new YoutubeResponseTransformer(), $fakeKey,
            'etag,items(id,snippet/title)');

        $result = $youtubeRepository->getByCountry('de');

        $this->assertSame($result, []);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function third_party_good_response_but_empty_items()
    {
        $faker = Factory::create();

        $fakeEtag = $faker->md5;
        $fakeKey = $faker->md5;

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(200, [], json_encode([
                'etag' => $fakeEtag,
                'items' => []
            ]))
        );

        $youtubeRepository = new YoutubeRepository($client, new YoutubeResponseTransformer(), $fakeKey,
            'etag,items(id,snippet/title)');

        $result = $youtubeRepository->getByCountry('de');

        $this->assertEquals($result['etag'], $fakeEtag);
        $this->assertSame($result['data'], []);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function get_by_country_and_etag_valid()
    {
        $faker = Factory::create();

        $fakeKey = $faker->md5;

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(304)
        );

        $youtubeRepository = new YoutubeRepository($client, new YoutubeResponseTransformer(), $fakeKey,
            'etag,items(id,snippet/title)');

        $result = $youtubeRepository->getByCountry('de');

        $this->assertEquals($result, null);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function returned_bad_response_code_from_third_party()
    {
        $faker = Factory::create();

        $fakeKey = $faker->md5;

        $badStatusCode = $faker->numberBetween(400, 599);

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response($badStatusCode)
        );

        $youtubeRepository = new YoutubeRepository($client, new YoutubeResponseTransformer(), $fakeKey,
            'etag,items(id,snippet/title)');

        $this->expectException(RepositoryException::class);
        $this->expectExceptionCode($badStatusCode);

        $youtubeRepository->getByCountry('de');
    }

    /**
     * @test
     * @throws RepositoryException
     * @dataProvider dataProvider
     */
    public function get_by_country_invalid($country)
    {
        $faker = Factory::create();

        $fakeKey = $faker->md5;

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(0)->andReturn();

        $youtubeRepository = new YoutubeRepository($client, new YoutubeResponseTransformer(), $fakeKey,
            'etag,items(id,snippet/title)');

        $this->expectException(RepositoryException::class);

        $youtubeRepository->getByCountry($country);
    }

    public function dataProvider()
    {
        return [
            // Invalid country name
            ['1dsa231'],
            // Empty country name
            [''],
        ];
    }
}