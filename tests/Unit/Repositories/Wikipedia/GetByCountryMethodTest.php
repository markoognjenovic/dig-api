<?php

namespace Tests\Unit\Repositories\Wikipedia;

use App\Exceptions\RepositoryException;
use App\Repositories\WikipediaRepository;
use App\Transformers\WikipediaResponseTransformer;
use Faker\Factory;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery;
use Tests\TestCase;

class GetByCountryMethodTest extends TestCase
{
    /**
     * @test
     * @throws RepositoryException
     */
    public function get_by_country_valid()
    {
        $faker = Factory::create();

        $fakeEtag = $faker->md5;
        $fakeData = $faker->realText(3000);

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(200, ['etag' => $fakeEtag], json_encode([
                'extract' => $fakeData
            ]))
        );

        $wikipediaRepository = new WikipediaRepository($client, new WikipediaResponseTransformer(),
            ['de' => 'Germany']);

        $result = $wikipediaRepository->getByCountry('de');

        $this->assertEquals($result['etag'], $fakeEtag);
        $this->assertEquals($result['data'], $fakeData);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function third_party_good_response_but_empty()
    {
        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(200, [], json_encode([]))
        );

        $wikipediaRepository = new WikipediaRepository($client, new WikipediaResponseTransformer(),
            ['de' => 'Germany']);

        $result = $wikipediaRepository->getByCountry('de');

        $this->assertSame($result, []);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function get_by_country_and_etag_valid()
    {
        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response(304)
        );

        $wikipediaRepository = new WikipediaRepository($client, new WikipediaResponseTransformer(),
            ['de' => 'Germany']);

        $result = $wikipediaRepository->getByCountry('de');

        $this->assertEquals($result, null);
    }

    /**
     * @test
     * @throws RepositoryException
     */
    public function returned_bad_response_code_from_third_party()
    {
        $faker = Factory::create();

        $badStatusCode = $faker->numberBetween(400, 600);

        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(1)->andReturn(
            new Response($badStatusCode)
        );

        $wikipediaRepository = new WikipediaRepository($client, new WikipediaResponseTransformer(),
            ['de' => 'Germany']);

        $this->expectException(RepositoryException::class);
        $this->expectExceptionCode($badStatusCode);

        $wikipediaRepository->getByCountry('de');
    }

    /**
     * @test
     * @throws RepositoryException
     * @dataProvider dataProvider
     */
    public function get_by_country_invalid($country)
    {
        $client = Mockery::mock(Client::class);
        $client->shouldReceive('request')->times(0)->andReturn();

        $wikipediaRepository = new WikipediaRepository($client, new WikipediaResponseTransformer(),
            ['de' => 'Germany']);

        $this->expectException(RepositoryException::class);

        $wikipediaRepository->getByCountry($country);
    }

    public function dataProvider()
    {
        return [
            // Invalid country name
            ['1dsa231'],
            // Empty country name
            [''],
        ];
    }

}