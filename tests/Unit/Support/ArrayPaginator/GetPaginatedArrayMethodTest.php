<?php

namespace Tests\Unit\Support\ArrayPaginator;


use App\Support\ArrayPaginator;
use Tests\TestCase;

class GetPaginatedArrayMethodTest extends TestCase
{
    protected $array;

    public function setUp(): void
    {
        parent::setUp();

        $this->array = [2, 3, 1, 5, 7];
    }

    /**
     * @test
     */
    public function get_pagination_as_requested()
    {
        $paginator = new ArrayPaginator($this->array, count($this->array), 0);

        $this->assertSame($paginator->getPaginatedArray(), $this->array);
    }

    /**
     * @test
     */
    public function get_pagination_on_empty_array()
    {
        $paginator = new ArrayPaginator([], 5, 0);

        $this->assertSame($paginator->getPaginatedArray(), []);
    }

    /**
     * @test
     */
    public function get_pagination_limit_larger_than_array()
    {
        $paginator = new ArrayPaginator($this->array, 10, 0);

        $this->assertSame($paginator->getPaginatedArray(), $this->array);
    }

    /**
     * @test
     */
    public function get_pagination_limit_negative()
    {
        $paginator = new ArrayPaginator($this->array, -1, 0);

        $this->assertSame($paginator->getPaginatedArray(), $this->array);
    }

    /**
     * @test
     */
    public function get_pagination_offset_larger_than_array()
    {
        $paginator = new ArrayPaginator($this->array, count($this->array), 10);

        $this->assertSame($paginator->getPaginatedArray(), []);
    }

    /**
     * @test
     */
    public function get_pagination_offset_negative()
    {
        $paginator = new ArrayPaginator($this->array, count($this->array), -1);

        $this->assertSame($paginator->getPaginatedArray(), $this->array);
    }

}