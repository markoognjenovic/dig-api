<?php

namespace Tests\Unit\Transformers\Youtube;


use App\Transformers\YoutubeResponseTransformer;
use Faker\Factory;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class TransformMethodTest extends TestCase
{
    /**
     * @test
     */
    public function transform_response_valid()
    {
        $faker = Factory::create();

        $fakeEtag = $faker->md5;
        $fakeId = $faker->uuid;
        $fakeJson = [
            'etag' => $fakeEtag,
            'items' => [
                [
                    'id' => $fakeId
                ]
            ]
        ];

        $response = (new YoutubeResponseTransformer())->transform(new Response(200, [], json_encode($fakeJson)));

        $this->assertEquals($response['etag'], $fakeEtag);
        $this->assertSame($response['data'], $fakeJson['items']);
    }

    /**
     * @test
     */
    public function transform_response_missing_etag()
    {
        $faker = Factory::create();
        $fakeId = $faker->uuid;
        $fakeJson = [
            'items' => [
                [
                    'id' => $fakeId
                ]
            ]
        ];

        $response = (new YoutubeResponseTransformer())->transform(new Response(200, [], json_encode($fakeJson)));

        $this->assertTrue(!isset($response['etag']));
        $this->assertSame($response['data'], $fakeJson['items']);
    }

    /**
     * @test
     */
    public function transform_response_missing_data()
    {
        $faker = Factory::create();
        $fakeEtag = $faker->md5;
        $fakeJson = [
            'etag' => $fakeEtag,
            'items' => []
        ];

        $response = (new YoutubeResponseTransformer())->transform(new Response(200, [], json_encode($fakeJson)));

        $this->assertEquals($response['etag'], $fakeEtag);
        $this->assertSame($response['data'], []);
    }

    /**
     * @test
     */
    public function transform_response_missing_both()
    {
        $response = (new YoutubeResponseTransformer())->transform(new Response(200));

        $this->assertSame($response, []);
    }
}