<?php

namespace Tests\Unit\Transformers\Wikipedia;


use App\Transformers\WikipediaResponseTransformer;
use Faker\Factory;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class TransformMethodTest extends TestCase
{
    /**
     * @test
     */
    public function transform_response_valid()
    {
        $faker = Factory::create();

        $fakeEtag = $faker->md5;
        $fakeForeword = $faker->realText(3000);
        $fakeJson = [
            'foo' => 'bar',
            'extract' => $fakeForeword
        ];

        $response = (new WikipediaResponseTransformer())->transform(new Response(200, ['etag' => $fakeEtag],
            json_encode($fakeJson)));

        $this->assertEquals($response['etag'], $fakeEtag);
        $this->assertSame($response['data'], $fakeForeword);
    }

    /**
     * @test
     */
    public function transform_response_missing_etag()
    {
        $faker = Factory::create();

        $fakeForeword = $faker->realText(3000);
        $fakeJson = [
            'foo' => 'bar',
            'extract' => $fakeForeword
        ];

        $response = (new WikipediaResponseTransformer())->transform(new Response(200, [], json_encode($fakeJson)));

        $this->assertTrue(!isset($response['etag']));
        $this->assertSame($response['data'], $fakeForeword);
    }

    /**
     * @test
     */
    public function transform_response_missing_data()
    {
        $faker = Factory::create();

        $fakeEtag = $faker->md5;

        $response = (new WikipediaResponseTransformer())->transform(new Response(200, ['etag' => $fakeEtag]));

        $this->assertEquals($response['etag'], $fakeEtag);
        $this->assertTrue(!isset($response['data']));
    }

    /**
     * @test
     */
    public function transform_response_missing_both()
    {
        $response = (new WikipediaResponseTransformer())->transform(new Response(200));

        $this->assertSame($response, []);
    }
}