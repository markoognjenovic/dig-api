<?php

namespace Tests\Unit\Controllers\Main;


use App\Http\Controllers\V1\MainController;
use App\Services\DataFetcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;
use Mockery;

class GetMethodTest extends TestCase
{
    /**
     * @test
     */
    public function get_request_valid()
    {
        $countries = [['country' => 'it'], ['country' => 'de']];

        Config::set('app.countries', ['it', 'de']);
        $dataFetcher = Mockery::mock(DataFetcher::class);
        $dataFetcher->shouldReceive('fetch')->once()
            ->andReturn($countries);

        $controller = new MainController();
        $response = $controller->get(new Request(), $dataFetcher);

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertSame($data['data'], $countries);
    }

}