<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Services\DataFetcher;
use App\Support\ArrayPaginator;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function get(Request $request, DataFetcher $dataFetcher)
    {
        $params = $this->validateGet($request);

        $paginator = new ArrayPaginator(
            config('app.countries'), $params['limit'] ?? null, $params['offset'] ?? null
        );

        return response()->json([
            'pagination' => $paginator->paginationData(),
            'data' => $dataFetcher->fetch($paginator->getPaginatedArray())
        ]);
    }

    private function validateGet(Request $request)
    {
        return $this->validate($request, [
            'limit' => ['integer', 'min:1'],
            'offset' => ['integer', 'min:0']
        ]);
    }

}
