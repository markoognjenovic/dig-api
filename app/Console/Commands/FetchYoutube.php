<?php

namespace App\Console\Commands;

use App\Contracts\YoutubeRepositoryInterface;
use App\Contracts\RedisRepositoryInterface;
use Illuminate\Console\Command;

class FetchYoutube extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:youtube {countries?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch most popular videos by country from YouTube.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(YoutubeRepositoryInterface $youtube, RedisRepositoryInterface $redis)
    {
        $results = [];

        $countries = !empty($this->argument("countries")) ?
            array_intersect($this->argument("countries"), config('app.countries')) : config('app.countries');

        foreach ($countries as $country) {

            if (!is_null($result = $youtube->getByCountry($country, $redis->getYoutubeEtag($country)))) {
                $results[$country] = $result;
            }

        }

        $redis->storeYoutube($results);

        $this->info("Youtube data fetched successfully.");
    }
}