<?php

namespace App\Console\Commands;

use App\Contracts\WikipediaRepositoryInterface;
use App\Contracts\RedisRepositoryInterface;
use Illuminate\Console\Command;

class FetchWikipedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:wikipedia {countries?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch foreword by country.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(WikipediaRepositoryInterface $wikipedia, RedisRepositoryInterface $redis)
    {
        $results = [];

        $countries = !empty($this->argument("countries")) ?
            array_intersect($this->argument("countries"), config('app.countries')) : config('app.countries');

        foreach ($countries as $country) {

            if (!is_null($result = $wikipedia->getByCountry($country, $redis->getWikipediaEtag($country)))) {
                $results[$country] = $result;
            }

        }

        $redis->storeWikipedia($results);

        $this->info("Wikipedia data fetched successfully.");
    }
}