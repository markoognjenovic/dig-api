<?php

namespace App\Console;

use App\Console\Commands\FetchWikipedia;
use App\Console\Commands\FetchYoutube;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FetchWikipedia::class,
        FetchYoutube::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('fetch:youtube')->hourly();
        $schedule->command('fetch:wikipedia')->daily();
    }
}
