<?php

namespace App\Services;


use App\Contracts\RedisRepositoryInterface;
use Illuminate\Support\Facades\Artisan;

class DataFetcher
{
    private $redisRepository;

    /**
     * DataFetcher constructor.
     * @param RedisRepositoryInterface $redisRepository
     */
    public function __construct(RedisRepositoryInterface $redisRepository)
    {
        $this->redisRepository = $redisRepository;
    }

    /**
     * @param array
     * @return array
     */
    public function fetch(array $countries): array
    {
        $results = array_filter($this->redisRepository->getByCountries($countries), array($this, 'isCountryDataValid'));

        $missingCountries = array_diff($countries, array_keys($results));

        if (!empty($missingCountries)) {
            $this->redisRepository->clearDataForCountries($missingCountries);

            Artisan::call('fetch:youtube', ['countries' => $missingCountries]);
            Artisan::call('fetch:wikipedia', ['countries' => $missingCountries]);

            $results = $this->sortResultsByCountryOrder(
                $results + $this->redisRepository->getByCountries($missingCountries), $countries
            );
        }

        return array_values($results);
    }

    /**
     * @param array $results
     * @param array $countries
     * @return array
     */
    private function sortResultsByCountryOrder(array $results, array $countries): array
    {
        $sorted = [];

        foreach ($countries as $key) {
            $sorted[$key] = $results[$key];
        }

        return $sorted;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isCountryDataValid(array $data): bool
    {
        return !empty($data['foreword']) && !empty($data['videos']);
    }
}