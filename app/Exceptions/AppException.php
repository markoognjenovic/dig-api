<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class AppException
 *
 * @package App\Exceptions
 */
class AppException extends Exception
{
    /**
     * AppException constructor.
     *
     * @param string $message
     * @param int $code
     */
    public function __construct($message = '', int $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR)
    {
        parent::__construct($message, $code);
    }
}
