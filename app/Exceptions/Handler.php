<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ValidationException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception): JsonResponse
    {
        if ($exception instanceof NotFoundHttpException) {

            $data = ['message' => 'Invalid url.'];
            $status = JsonResponse::HTTP_NOT_FOUND;

        } elseif ($exception instanceof HttpException) {

            $data = ['message' => $exception->getMessage()];
            $status = $exception->getStatusCode();

        } elseif ($exception instanceof AppException) {

            $data = ['message' => $exception->getMessage()];
            $status = $exception->getCode();

        } elseif ($exception instanceof ValidationException) {

            $data = [
                'message' => $exception->getMessage(),
                'errors' => $exception->errors(),
            ];
            $status = JsonResponse::HTTP_UNPROCESSABLE_ENTITY;

        } else {

            $data = ['message' => $exception->getMessage()];
            $status = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        return response()->json($data, $status);

    }
}
