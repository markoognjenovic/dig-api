<?php

namespace App\Support;


class ArrayPaginator
{
    protected $total;
    protected $numberOfResults;
    protected $array;

    /**
     * ArrayPaginator constructor.
     * @param array $array
     * @param int|null $max
     * @param int|null $limit
     * @param int|null $offset
     */
    public function __construct(array $array, ?int $limit, ?int $offset)
    {
        $this->total = count($array);

        $limit = isset($limit) && $limit > 0 ? $limit : $this->total;
        $offset = max(0, $offset ?? 0);

        $this->array = array_slice($array, $offset, $limit);
        $this->numberOfResults = count($this->array);
    }

    /**
     * @return array
     */
    public function getPaginatedArray(): array
    {
        return $this->array;
    }

    /**
     * @return array
     */
    public function paginationData(): array
    {
        return [
            'total' => $this->total,
            'number_of_results' => $this->numberOfResults,
        ];
    }

}