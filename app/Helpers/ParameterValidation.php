<?php

namespace App\Helpers;


trait ParameterValidation
{
    private function isCountryValid(string $name): bool
    {
        return strlen($name) == 2;
    }
}