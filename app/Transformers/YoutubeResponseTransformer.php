<?php

namespace App\Transformers;


use App\Contracts\ResponseTransformerInterface;
use GuzzleHttp\Psr7\Response;

class YoutubeResponseTransformer implements ResponseTransformerInterface
{
    /**
     * @param Response $response
     * @return array
     */
    public function transform(Response $response): array
    {
        $changes = [];

        $data = json_decode($response->getBody(), true);

        if (isset($data['etag'])) {
            $changes['etag'] = $data['etag'];
        }

        if (isset($data['items'])) {
            $changes['data'] = $data['items'];
        }

        return $changes;
    }
}