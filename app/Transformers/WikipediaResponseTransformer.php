<?php

namespace App\Transformers;


use App\Contracts\ResponseTransformerInterface;
use GuzzleHttp\Psr7\Response;

class WikipediaResponseTransformer implements ResponseTransformerInterface
{
    /**
     * @param Response $response
     * @return array
     */
    public function transform(Response $response): array
    {
        $changes = [];

        if ($response->hasHeader('etag') && count($response->getHeader("etag")) > 0) {
            $changes['etag'] = $response->getHeader('etag')[0];
        }

        $data = json_decode($response->getBody(), true);

        if (isset($data['extract'])) {
            $changes['data'] = $data['extract'];
        }

        return $changes;
    }
}