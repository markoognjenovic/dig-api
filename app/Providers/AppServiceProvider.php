<?php

namespace App\Providers;

use App\Contracts\WikipediaRepositoryInterface;
use App\Contracts\YoutubeRepositoryInterface;
use App\Repositories\RedisRepository;
use App\Contracts\RedisRepositoryInterface;
use App\Repositories\WikipediaRepository;
use App\Repositories\YoutubeRepository;
use App\Services\DataFetcher;
use App\Transformers\WikipediaResponseTransformer;
use App\Transformers\YoutubeResponseTransformer;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->repositories();
        $this->services();
    }

    /**
     * @return void
     */
    private function repositories(): void
    {
        $this->app->bind(RedisRepositoryInterface::class, function () {
            return new RedisRepository(
                $this->app->make('redis')
            );
        });

        $this->app->bind(YoutubeRepositoryInterface::class, function () {
            return new YoutubeRepository(
                $this->app->make(Client::class, [
                    'config' => [
                        'base_uri' => config('youtube.base_url')
                    ]
                ]),
                $this->app->make(YoutubeResponseTransformer::class),
                config('youtube.api_key'),
                config('youtube.fields')
            );
        });

        $this->app->bind(WikipediaRepositoryInterface::class, function () {
            return new WikipediaRepository(
                $this->app->make(Client::class, [
                    'config' => [
                        'base_uri' => config('wikipedia.base_url')
                    ]
                ]),
                $this->app->make(WikipediaResponseTransformer::class),
                config('wikipedia.articles')
            );
        });
    }

    /**
     * @return void
     */
    private function services(): void
    {
        $this->app->bind(DataFetcher::class, function () {
            return new DataFetcher(
                $this->app->make(RedisRepositoryInterface::class)
            );
        });
    }
}
