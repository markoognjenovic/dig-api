<?php

namespace App\Contracts;


interface RedisRepositoryInterface
{
    public function storeYoutube(array $data): void;

    public function storeWikipedia(array $data): void;

    public function getYoutubeEtag(string $country): ? string;

    public function getWikipediaEtag(string $country): ? string;

    public function getByCountries(array $countries): array;

    public function clearDataForCountries(array $countries): void;
}