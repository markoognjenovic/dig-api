<?php

namespace App\Contracts;


interface ThirdPartyRepositoryInterface
{
    public function getByCountry(string $country, string $etag = null): ? array;
}