<?php

namespace App\Contracts;


use GuzzleHttp\Psr7\Response;

interface ResponseTransformerInterface
{
    public function transform(Response $response): array;
}