<?php

namespace App\Repositories;

use App\Exceptions\RepositoryException;
use App\Contracts\ResponseTransformerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

abstract class ThirdPartyRepository
{
    protected $client;
    protected $transformer;

    /**
     * ThirdPartyRepository constructor.
     * @param $client
     * @param $transformer
     */
    public function __construct(Client $client, ResponseTransformerInterface $transformer)
    {
        $this->client = $client;
        $this->transformer = $transformer;
    }

    /**
     * @param Response $response
     * @return array
     * @throws RepositoryException
     */
    protected function handleResponse(Response $response): ? array
    {
        if ($response->getStatusCode() == 200) {
            return $this->transformedResponse($response);
        }

        if ($response->getStatusCode() == 304) {
            return null;
        }

        throw new RepositoryException($response->getReasonPhrase(), $response->getStatusCode());
    }

    /**
     * @param Response $response
     * @return array
     */
    protected function transformedResponse(Response $response): array
    {
        return $this->transformer->transform($response);
    }
}