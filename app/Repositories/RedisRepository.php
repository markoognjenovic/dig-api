<?php

namespace App\Repositories;


use App\Contracts\RedisRepositoryInterface;
use Illuminate\Redis\RedisManager;

class RedisRepository implements RedisRepositoryInterface
{

    protected $client;

    /**
     * RedisRepository constructor.
     * @param RedisManager $client
     */
    public function __construct(RedisManager $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $data
     */
    public function storeYoutube(array $data): void
    {
        foreach ($data as $country => $content) {
            if (isset($content['etag'])) {
                $this->storeYoutubeEtag($country, $content['etag']);
            }

            $this->storeYoutubeVideos($country, $content['data'] ?? []);
        }
    }

    /**
     * @param string $country
     * @param string $etag
     */
    private function storeYoutubeEtag(string $country, string $etag): void
    {
        $this->client->hset("country:" . $country, "videos:etag", $etag);
    }

    /**
     * @param string $country
     * @param array $videos
     */
    private function storeYoutubeVideos(string $country, array $videos): void
    {
        $this->client->hset("country:" . $country, "videos", json_encode($videos));
    }

    /**
     * @param array $data
     */
    public function storeWikipedia(array $data): void
    {
        foreach ($data as $country => $content) {
            if (isset($content['etag'])) {
                $this->storeWikipediaEtag($country, $content['etag']);
            }

            $this->storeWikipediaForeword($country, $content['data'] ?? "");
        }
    }

    /**
     * @param string $country
     * @param string $etag
     */
    private function storeWikipediaEtag(string $country, string $etag): void
    {
        $this->client->hset("country:" . $country, "foreword:etag", $etag);
    }

    /**
     * @param string $country
     * @param string $foreword
     */
    private function storeWikipediaForeword(string $country, string $foreword): void
    {
        $this->client->hset("country:" . $country, "foreword", $foreword);
    }

    /**
     * @param string $country
     * @return null|string
     */
    public function getYoutubeEtag(string $country): ? string
    {
        return $this->client->hget("country:" . $country, "videos:etag");
    }

    /**
     * @param string $country
     * @return null|string
     */
    public function getWikipediaEtag(string $country): ? string
    {
        return $this->client->hget("country:" . $country, "foreword:etag");
    }

    /**
     * @param array $countries
     * @return array
     */
    public function getByCountries(array $countries): array
    {
        $results = [];

        foreach ($countries as $country) {
            $singleCountry = [];

            $singleCountry['country'] = $country;
            $singleCountry['foreword'] = $this->getForewordByCountry($country);
            $singleCountry['videos'] = $this->getVideosByCountry($country);

            $results[$country] = $singleCountry;
        }

        return $results;
    }

    /**
     * @param string $country
     * @return null|string
     */
    private function getForewordByCountry(string $country): ? string
    {
        return $this->client->hget("country:" . $country, "foreword");
    }

    /**
     * @param string $country
     * @return array|null
     */
    private function getVideosByCountry(string $country): ? array
    {
        return json_decode($this->client->hget("country:" . $country, "videos"), true);
    }

    /**
     * @param array $countries
     */
    public function clearDataForCountries(array $countries): void
    {
        $keys = [];

        foreach ($countries as $country) {
            $keys[] = 'country:' . $country;
        }

        $this->client->del($keys);
    }

}