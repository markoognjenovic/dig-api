<?php

namespace App\Repositories;


use App\Contracts\WikipediaRepositoryInterface;
use App\Exceptions\RepositoryException;
use App\Helpers\ParameterValidation;
use App\Transformers\WikipediaResponseTransformer;
use GuzzleHttp\Client;

class WikipediaRepository extends ThirdPartyRepository implements WikipediaRepositoryInterface
{
    use ParameterValidation;

    protected $articles;

    /**
     * WikipediaRepository constructor.
     * @param Client $client
     * @param WikipediaResponseTransformer $transformer
     * @param array $articles
     */
    public function __construct(
        Client $client,
        WikipediaResponseTransformer $transformer,
        array $articles
    ) {
        parent::__construct($client, $transformer);
        $this->articles = $articles;
    }

    /**
     * @return array
     * @throws RepositoryException
     */
    public function getByCountry(string $country, string $etag = null): ? array
    {
        $params = [
            'verify' => false
        ];

        if (!$this->isCountryValid($country)) {
            throw new RepositoryException("Invalid country provided.");
        }

        if (!empty($etag)) {
            $params['headers'] = [
                'If-None-Match' => $etag
            ];
        }

        if (!isset($this->articles[$country])) {
            throw new RepositoryException("Article for country " . strtoupper($country) . " is missing.");
        }

        $response = $this->client->request('GET', 'page/summary/' . $this->articles[$country], $params);

        return $this->handleResponse($response);
    }


}