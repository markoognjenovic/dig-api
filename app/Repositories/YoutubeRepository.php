<?php

namespace App\Repositories;

use App\Contracts\YoutubeRepositoryInterface;
use App\Exceptions\RepositoryException;
use App\Helpers\ParameterValidation;
use App\Transformers\YoutubeResponseTransformer;
use GuzzleHttp\Client;

class YoutubeRepository extends ThirdPartyRepository implements YoutubeRepositoryInterface
{
    use ParameterValidation;

    protected $apiKey;
    protected $fields;

    /**
     * YoutubeRepository constructor.
     * @param Client $client
     * @param YoutubeResponseTransformer $transformer
     * @param string $apiKey
     * @param string $fields
     * @throws RepositoryException
     */
    public function __construct(
        Client $client,
        YoutubeResponseTransformer $transformer,
        string $apiKey,
        string $fields
    ) {
        parent::__construct($client, $transformer);

        if (empty($apiKey)) {
            throw new RepositoryException("Youtube API key is not provided.");
        }

        $this->apiKey = $apiKey;

        if (empty($fields)) {
            throw new RepositoryException("Youtube fields parameter can't be empty.");
        }

        $this->fields = $fields;
    }

    /**
     * @return array
     * @throws RepositoryException
     */
    public function getByCountry(string $country, string $etag = null): ? array
    {
        $params = [
            'query' => [
                'key' => $this->apiKey,
                'part' => 'snippet',
                'chart' => 'mostPopular',
                'fields' => $this->fields,
                'regionCode' => $country
            ],
            'verify' => false
        ];

        if (!$this->isCountryValid($country)) {
            throw new RepositoryException("Invalid country provided.");
        }

        if (!empty($etag)) {
            $params['headers'] = [
                'If-None-Match' => $etag
            ];
        }

        $response = $this->client->request('GET', 'videos', $params);

        return $this->handleResponse($response);
    }
}